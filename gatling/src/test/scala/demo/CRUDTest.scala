package demo

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class CRUDTest extends Simulation {

  val maxUsers = 1
  val baseUrl = "http://localhost:8081/resources"
  val nextId = { var i = 0; () => { i += 1; i} }
  val duration = 60 * 3
  val name = "Resource"


  def randomData = {
    var name = "PerfTestYetAnotherFileName-" + nextId().toString

    var data = """{
           "name": "%s"
           }""" format(name)

    data
  }

  val httpConf = http
    .baseURL(baseUrl)
    .acceptHeader("application/json")
    .headers(Util.json)
    .doNotTrackHeader("1")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")
    .connection("keep-alive")


  val feeder = Iterator.continually(Map("resource" -> randomData))

  val scn = scenario(name).during(duration) {
    feed(feeder)
      .exec(http("Post Resource")
        .post("/")
        .body(StringBody("${resource}"))
        .check(status.is(201))
        .check(jsonPath("$.id").saveAs("id"))
      ).pause(1)
      .exec(http("Get Resource")
        .get("/${id}")
        .check(status.is(200))
      ).pause(2)
      .exec(http("Delete Resource")
        .delete("/${id}")
        .check(status.is(204))
      ).pause(500 millis)
  }

  setUp(
    scn.inject(rampUsers(maxUsers) over (30 seconds))
  ).protocols(httpConf)

}
