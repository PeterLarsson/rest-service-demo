/*
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of sklintyg (https://github.com/sklintyg).
 *
 * sklintyg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sklintyg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package demo

import io.gatling.core.Predef._
import io.gatling.http.Predef.http

import scala.concurrent.duration._

class LoadTest extends Simulation {

  val maxUsers = 900

  val baseUrl = "http://localhost:8081/resources"
  val crud = new CRUDTest

  val bootJVM = http
    .baseURL("http://localhost:8081/resources")
    .acceptHeader("application/json")
    .headers(Util.json)
    .doNotTrackHeader("1")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")
    .connection("keep-alive")

  val bootGVM = http
    .baseURL("http://localhost:8082/resources")
    .acceptHeader("application/json")
    .headers(Util.json)
    .doNotTrackHeader("1")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")
    .connection("keep-alive")

  val mnJVM = http
    .baseURL("http://localhost:9091/resources")
    .acceptHeader("application/json")
    .headers(Util.json)
    .doNotTrackHeader("1")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")
    .connection("keep-alive")

  val mnGVM = http
    .baseURL("http://localhost:9092/resources")
    .acceptHeader("application/json")
    .headers(Util.json)
    .doNotTrackHeader("1")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")
    .connection("keep-alive")

  setUp(
    crud.scn.inject(rampUsers(maxUsers) over (60 seconds)).protocols(mnGVM)
  )

}
