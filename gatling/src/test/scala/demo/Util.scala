package demo

object Util {

  val acceptJson = """application/json, text/plain, */*"""

  val json = Map(
    "Accept" -> acceptJson,
    "Content-Type" -> """application/json;charset=UTF-8""")
}
