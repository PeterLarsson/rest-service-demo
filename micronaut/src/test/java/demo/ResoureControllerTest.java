package demo;

import demo.dto.Resource;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;
import io.micronaut.context.ApplicationContext;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.runtime.server.EmbeddedServer;
import java.nio.charset.Charset;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ResoureControllerTest {

    private static EmbeddedServer server;
    private static HttpClient client;

    @BeforeClass
    public static void setupServer() {
        server = ApplicationContext.run(EmbeddedServer.class); // <1>
        client = server
                .getApplicationContext()
                .createBean(HttpClient.class, server.getURL());  // <2>
    }

    @AfterClass
    public static void stopServer() {
        if (server != null) {
            server.stop();
        }
        if (client != null) {
            client.stop();
        }
    }

    @Test
    public void testResourceCrudOperations() {
        final String cp = "/resources";
        Resource source = randomize(Resource.class, "id");
        HttpRequest request = HttpRequest.POST(cp, source);
        HttpResponse response = client.toBlocking().exchange(request);
        assertEquals(HttpStatus.CREATED, response.getStatus());
        assertNotNull(response.header("Location"));

        request = HttpRequest.GET(response.header("Location"));
        response = client.toBlocking().exchange(request);
        assertEquals(HttpStatus.OK, response.getStatus());
    }

    static EnhancedRandom random = EnhancedRandomBuilder.aNewEnhancedRandomBuilder()
            .randomizationDepth(10)
            .charset(Charset.forName("utf8"))
            .scanClasspathForConcreteTypes(true)
            .build();

    public <T> T randomize(final Class<T> type, final String... excludedFields) {
        return random.nextObject(type, excludedFields);
    }
}
