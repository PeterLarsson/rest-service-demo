package demo;

import demo.dto.Resource;
import demo.model.EDMapper;
import demo.model.ResourceEntity;
import demo.model.ResourceRepository;
import io.micronaut.core.util.StringUtils;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.validation.Validated;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Validated
@Controller("/resources")
public class ResoureController {

    static Logger LOG = LoggerFactory.getLogger(ResoureController.class);


    final ResourceRepository resourceRepository;
    final EDMapper edMapper;

    public ResoureController(ResourceRepository resourceRepository, EDMapper edMapper) {
        this.resourceRepository = resourceRepository;
        this.edMapper = edMapper;
    }

    @Get("/{id}")
    public HttpResponse<Resource> getResource(String id) {

        Optional<ResourceEntity> ro = resourceRepository.findById(id);

        return ro.isPresent()
                ? HttpResponse.ok(edMapper.dto(ro.get()))
                : HttpResponse.notFound();
    }

    @Get("/")
    public List<Resource> getResourcesByName(@QueryValue(value = "name") String name) {
        return edMapper.dto(StringUtils.isEmpty(name) ? resourceRepository.findAll()
                : resourceRepository.findByNameIn(Arrays.asList(name.split(","))));
    }

    @Post
    public HttpResponse<Resource> postResource(Resource resource) {
        final Resource r = edMapper.dto(resourceRepository.save(edMapper.entity(resource)));
        return HttpResponse.created(r)
                .headers(headers -> headers.location(location(r.getId())));
    }

    @Delete("/{id}")
    public HttpResponse deleteResource(String id) {
        resourceRepository.delete(id);
        return HttpResponse.noContent();
    }


    protected URI location(String id) {
        return URI.create("/resources/" + id);
    }
}
