package demo.model;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ResourceRepository {

    List<ResourceEntity> findAll();

    Optional<ResourceEntity> findById(String id);

    List<ResourceEntity> findByNameIn(Collection<String> name);

    ResourceEntity save(ResourceEntity resource);

    void delete(String id);
}
