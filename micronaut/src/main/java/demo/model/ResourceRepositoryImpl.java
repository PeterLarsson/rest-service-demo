package demo.model;

import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.spring.tx.annotation.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceRepositoryImpl implements ResourceRepository {

    static final String ALL_QRY = "SELECT r FROM ResourceEntity as r";
    static final String NAME_QRY = ALL_QRY + " WHERE name in :names";


    @PersistenceContext
    private EntityManager entityManager;


    public ResourceRepositoryImpl(@CurrentSession EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ResourceEntity> findAll() {
        return query(ALL_QRY, ResourceEntity.class).getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ResourceEntity> findById(String id) {
        return Optional.of(entityManager.find(ResourceEntity.class, id));
    }

    @Override
    @Transactional(readOnly = true)
    public List<ResourceEntity> findByNameIn(Collection<String> names) {
        return query(NAME_QRY, ResourceEntity.class)
                .setParameter("names", names)
                .getResultList();
    }

    @Override
    @Transactional
    public ResourceEntity save(ResourceEntity resource) {
        entityManager.persist(resource);
        return resource;
    }

    @Override
    @Transactional
    public void delete(String id) {
        entityManager.remove(findById(id).get());
    }

    //
    <T> TypedQuery<T> query(String sql, Class<T> cls) {
        return entityManager.createQuery(sql, cls);
    }
}
