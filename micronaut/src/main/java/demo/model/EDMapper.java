package demo.model;

import demo.dto.Resource;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Singleton;

@Singleton
public class EDMapper {

    public Resource dto(ResourceEntity resourceEntity) {
        Resource r = new Resource();
        r.setId(resourceEntity.getId());
        r.setName(resourceEntity.getName());
        r.setCreatedDate(resourceEntity.getCreatedTime());
        r.setModifiedDate(resourceEntity.getModifiedTime());
        return r;
    }

    public List<Resource> dto(List<ResourceEntity> resourceEntities) {
        return resourceEntities.stream()
                .map(this::dto)
                .collect(Collectors.toList());
    }

    public ResourceEntity entity(Resource resource) {
        ResourceEntity e = new ResourceEntity();
        e.setId(resource.getId());
        e.setName(resource.getName());
        e.setCreatedTime(resource.getCreatedDate());
        e.setModifiedTime(resource.getModifiedDate());
        return e;
    }
}
