package demo.model;

import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "RESOURCE_ENTITY")
public class ResourceEntity {
    @Id
    private String id;

    private String name;

    @Column(updatable = false, nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createdTime;

    @Column(insertable = false)
    private LocalDateTime modifiedTime;


    @PrePersist
    void onPrePersist() {
        setId(UUID.randomUUID().toString());
        this.createdTime = LocalDateTime.now();
    }

    @PreUpdate
    void onPreUpdate() {
        this.modifiedTime = LocalDateTime.now();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public LocalDateTime getModifiedTime() {
        return modifiedTime;
    }

    void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    void setModifiedTime(LocalDateTime modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
