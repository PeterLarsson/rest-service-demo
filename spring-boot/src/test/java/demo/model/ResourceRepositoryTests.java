package demo.model;

import demo.TestSupport;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


import static org.assertj.core.api.Assertions.assertThat;

@Transactional
public class ResourceRepositoryTests extends TestSupport {

    @Autowired
    ResourceRepository resourceRepository;

    @Before
    public void before() {
        resourceRepository.deleteAll();
    }

    @Test
    public void saveShouldAssignIdAndCreatedTime() {
        ResourceEntity entity = randomize(ResourceEntity.class, "id", "createdTime", "modifiedTime");
        assertThat(entity.getCreatedTime()).isNull();
        ResourceEntity saved = resourceRepository.saveAndFlush(entity);
        assertThat(saved.getId()).isNotNull();
        assertThat(saved.getCreatedTime()).isNotNull();
    }

    @Test
    public void updateShouldAssignModifiedTime() {
        ResourceEntity entity = randomize(ResourceEntity.class, "id", "createdTime", "modifiedTime");
        ResourceEntity saved = resourceRepository.saveAndFlush(entity);
        assertThat(saved.getModifiedTime()).isNull();
        saved.setName("-");
        ResourceEntity updated = resourceRepository.saveAndFlush(saved);
        assertThat(updated.getModifiedTime()).isNotNull();
    }

}
