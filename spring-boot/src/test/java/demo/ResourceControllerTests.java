/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package demo;

import demo.dto.Resource;
import demo.model.ResourceRepository;
import java.util.List;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;


import static demo.ResourceController.RESOURCES;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class ResourceControllerTests extends TestSupport {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ResourceRepository resourceRepository;

    @Before
    public void before() {
        resourceRepository.deleteAll();
    }

    @SneakyThrows
    @Test
    public void postShouldReturnId() {
        postResource(randomize(Resource.class, "id"));
        assertThat(resourceRepository.count()).isEqualTo(1);
    }

    @SneakyThrows
    @Test
    public void getShouldReturnPostedResource() {
        Resource saved = postResource(randomize(Resource.class, "id"));

        MockHttpServletRequestBuilder rb = get(path(RESOURCES, saved.getId()))
                .accept(APPLICATION_JSON_UTF8);
        byte[] content = perform(rb, status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andReturn().getResponse().getContentAsByteArray();

        Resource retrieved = parse(content, Resource.class);

        assertThat(retrieved).isEqualTo(saved);
    }

    @Test
    public void deleteShouldRemoveObject() {
        Resource saved = postResource(randomize(Resource.class, "id"));
        MockHttpServletRequestBuilder rb = delete(path(RESOURCES, saved.getId()))
                .accept(APPLICATION_JSON_UTF8);
        perform(rb, status().isNoContent());
        assertThat(resourceRepository.count()).isEqualTo(0);
    }

    @Test
    public void findWithPredicate() {
        Resource r1 = postResource(randomize(Resource.class, "id"));
        Resource r2 = postResource(randomize(Resource.class, "id"));

        MockHttpServletRequestBuilder rb = get(path(RESOURCES)).param("name", r2.getName());
        byte[] content = perform(rb, status().isOk())
                .andReturn().getResponse().getContentAsByteArray();

        List<Resource> list = parseList(content, Resource.class);

        assertThat(list.size()).isEqualTo(1);
        assertThat(list.get(0)).isEqualToComparingFieldByFieldRecursively(r2);
    }

    @SneakyThrows
    Resource postResource(Resource resource) {
        MockHttpServletRequestBuilder rb = post(path(RESOURCES))
                .contentType(APPLICATION_JSON_UTF8)
                .content(dump(resource));

        byte[] body = perform(rb, status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andReturn().getResponse().getContentAsByteArray();

        return parse(body, Resource.class);
    }

    @SneakyThrows
    ResultActions perform(MockHttpServletRequestBuilder rb, ResultMatcher status) {
        return mockMvc.perform(rb).andDo(print()).andExpect(status);
    }

}
