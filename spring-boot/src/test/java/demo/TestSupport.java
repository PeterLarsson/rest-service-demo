package demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;
import java.nio.charset.Charset;
import java.util.List;
import lombok.SneakyThrows;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan
public abstract class TestSupport {

    @Autowired
    ObjectMapper jsonMapper;

    static EnhancedRandom random = EnhancedRandomBuilder.aNewEnhancedRandomBuilder()
            .randomizationDepth(10)
            .charset(Charset.forName("utf8"))
            .scanClasspathForConcreteTypes(true)
            .build();

    public <T> T randomize(final Class<T> type, final String... excludedFields) {
        return random.nextObject(type, excludedFields);
    }

    @SneakyThrows
    public  byte[] dump(final Object object) {
        return jsonMapper.writeValueAsBytes(object);
    }

    @SneakyThrows
    public <T> T parse(final byte[] bytes, final Class<T> type) {
        return jsonMapper.readValue(bytes, type);
    }

    @SneakyThrows
    public <T> List<T> parseList(final byte[] bytes, final Class<T> type) {
        return jsonMapper.readValue(bytes,
                jsonMapper.getTypeFactory().constructCollectionType(List.class, type));
    }

    // build path
    String path(String... elements) {
        final StringBuilder sb = new StringBuilder();
        for (final String e : elements) {
            sb.append("/").append(e);
        }
        return sb.toString();
    }

}
