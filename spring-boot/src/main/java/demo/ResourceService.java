package demo;

import com.querydsl.core.types.Predicate;
import demo.dto.Resource;
import demo.model.ResourceEntity;
import demo.model.ResourceRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ResourceService {

    @Autowired
    ResourceRepository resourceRepository;

    @Autowired
    EDMapper edMapper;

    public Resource create(Resource resource) {
        ResourceEntity entity = edMapper.entity(resource);
        return edMapper.dto(resourceRepository.save(entity));
    }

    public void delete(String id) {
        resourceRepository.deleteById(id);
    }

    public Resource get(String id) {
        Optional<ResourceEntity> entity = resourceRepository.findById(id);
        return entity.map(e -> edMapper.dto(e)).orElse(null);
    }

    public List<Resource> find(Predicate predicate) {
        return edMapper.dto(resourceRepository.findAll(predicate));
    }
}
