package demo.model;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceRepository extends JpaRepository<ResourceEntity, String>,
        QuerydslPredicateExecutor<ResourceEntity>,
        QuerydslBinderCustomizer<QResourceEntity> {

    List<ResourceEntity> findAll(Predicate predicate);

    @Override
    default void customize(QuerydslBindings bindings, QResourceEntity entity) {
        bindings.bind(entity.createdTimeFrom).first((path, value) -> entity.createdTime.goe(value));
        bindings.bind(entity.name).all((path, values) -> {
            BooleanBuilder predicate = new BooleanBuilder();
            values.forEach(v -> {
                predicate.or(path.containsIgnoreCase(v));
            });
            return Optional.of(predicate.getValue());
        });
    }
}

