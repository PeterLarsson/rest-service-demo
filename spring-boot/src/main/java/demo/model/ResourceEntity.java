package demo.model;

import com.querydsl.core.annotations.PropertyType;
import com.querydsl.core.annotations.QueryType;
import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Data
public class ResourceEntity {
    @Id
    private String id;
    private String name;
    @Column(updatable = false, nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createdTime;
    @Column(insertable = false)
    private LocalDateTime modifiedTime;

    // querydsl query attribute
    @Transient
    @QueryType(PropertyType.DATE)
    @Getter(value = AccessLevel.NONE) @Setter(value = AccessLevel.NONE)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createdTimeFrom;

    @PrePersist
    void onPrePersist() {
        setId(UUID.randomUUID().toString());
        setCreatedTime(LocalDateTime.now());
    }

    @PreUpdate
    void onPreUpdate() {
        setModifiedTime(LocalDateTime.now());
    }
}
