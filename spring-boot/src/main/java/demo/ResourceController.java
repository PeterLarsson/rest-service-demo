package demo;

import com.querydsl.core.types.Predicate;
import demo.dto.Resource;
import demo.model.ResourceEntity;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Slf4j
@RestController
@RequestMapping("api")
public class ResourceController {

    static final String RESOURCES = "resources";

    @Autowired
    ResourceService service;

    @Autowired
    PushService pushService;

    @Value(staticConstructor = "of")
    static class Notification {
         private String action;
         private String id;
    }

    @PostMapping(RESOURCES)
    public ResponseEntity<Resource> postResource(@RequestBody Resource resource) {
        Resource r = service.create(resource);
        pushService.notify("post", Notification.of("Created", r.getId()));
        return ResponseEntity.created(location(r.getId())).body(r);
    }

    @GetMapping(RESOURCES + "/{id}")
    public Resource getResource(@PathVariable String id) {
        return service.get(id);
    }

    @DeleteMapping(RESOURCES + "/{id}")
    public ResponseEntity deleteReource(@PathVariable String id) {
        service.delete(id);
        pushService.notify("delete", Notification.of("Delete", id));
        return ResponseEntity.noContent().build();
    }

    @GetMapping(RESOURCES)
    public List<Resource> getResources(@QuerydslPredicate(root = ResourceEntity.class) Predicate predicate) {
        return service.find(predicate);
    }

    @GetMapping("/sse")
    public SseEmitter streamEvents(@RequestParam("topic") List<String> topics) {
        return pushService.newSseEmitter(topics);
    }

    @GetMapping
    public Map<String, Object> home() {
        return Collections.singletonMap("status", 200);
    }

    protected URI location(String id) {
        return URI.create("/resources/" + id);
    }
}
