package demo;

import demo.dto.Resource;
import demo.model.ResourceEntity;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EDMapper {

    Resource dto(ResourceEntity resourceEntity);

    List<Resource> dto(List<ResourceEntity> resourceEntities);

    ResourceEntity entity(Resource resource);
}
