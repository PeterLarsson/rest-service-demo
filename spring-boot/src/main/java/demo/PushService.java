package demo;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Slf4j
@Service
public class PushService {

    @Autowired
    ApplicationEventPublisher eventPublisher;

    List<SseClient> sseClients = Lists.newArrayList();

    @Value(staticConstructor = "of")
    public static class Event<T> {
        private String topic;
        private T data;
        private LocalDateTime createdTime = LocalDateTime.now();
    }

    @Value(staticConstructor = "of")
    static class SseClient {
        static final AtomicInteger seqGenerator = new AtomicInteger();
        private Integer seqNo = seqGenerator.incrementAndGet();
        private List<String> topics;
        private SseEmitter sseEmitter;

        <T> boolean accept(Event<T> event) {
            return topics.isEmpty() || topics.contains(event.getTopic());
        }

        @SneakyThrows
        <T> void send(Event<T> event) {
            if (accept(event)) {
                getSseEmitter().send(event, MediaType.APPLICATION_JSON_UTF8);
            }
        }
    }

    public <T> void notify(final String topic, final T data) {
        eventPublisher.publishEvent(Event.of(topic, data));
    }

    public SseEmitter newSseEmitter(List<String>  topics) {
        final SseClient sseClient = SseClient.of(topics, new SseEmitter());
        sseClients.add(sseClient);
        sseClient.getSseEmitter().onCompletion(() -> sseClients.removeIf(c -> c.getSeqNo().equals(sseClient.getSeqNo())));
        sseClient.getSseEmitter().onTimeout(() -> {
            log.info("Client timeout for {}", sseClient);
            sseClients.removeIf(c -> c.getSeqNo().equals(sseClient.getSeqNo()));
            sseClient.getSseEmitter().complete();
        });
        return sseClient.getSseEmitter();
    }

    @EventListener(Event.class)
    void onEvent(Event<ResourceController.Notification> event) {
        final Set<Integer> staleSet = Sets.newHashSet();
        sseClients.forEach(client -> {
            try {
                log.info("Send to {}", client);
                client.send(event);
            } catch (Throwable e) {
                staleSet.add(client.getSeqNo());
                log.error("Unable to send to client: {}", rootOf(e).toString());
            }
        });
        if (!staleSet.isEmpty()) {
            sseClients.removeIf(client -> staleSet.contains(client.getSeqNo()));
        }
    }

    private Throwable rootOf(Throwable e) {
        final Throwable cause = e.getCause();
        return Objects.isNull(cause) ? e : cause;
    }
}
