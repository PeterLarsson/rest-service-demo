package demo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.time.LocalDateTime;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Resource {
    private String id;
    private String name;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
}
